var vpkr = artifacts.require("./vpkr.sol");

contract('vpkr',function(accounts) {

    it('Iiniializes the contracts with the correct values',function(){
        return vpkr.deployed().then(function(instance){
            tokenInstance = instance;
            return tokenInstance.name();
        }).then(function(name){
            assert.equal(name,'vPakistaniRupee','has the correct name');
            return tokenInstance.symbol();
        }).then(function(symbol){
            assert.equal(symbol,'VPKR','has the correct symbol');
            return tokenInstance.decimals();
        }).then(function(decimals){
            assert.equal(decimals,8,'The decimal value of the token is 8');
        });
    });

    it('allocates inital Coins to 1 Billion',function(){
    return vpkr.deployed().then(function(instance){
        tokenInstance = instance;
        return tokenInstance.totalSupply();
            }).then(function(totalSupply) {
                assert.equal(totalSupply.toNumber(),1000000000,'Set the total supply to 1 billion');
                return tokenInstance.balances(accounts[0]);
            }).then(function(adminBalance){
                assert.equal(adminBalance.toNumber(),1000000000,'It allocates initial supply to the admin account')
            });
        });
    
    it('Transfers the Ownership of the coins',function(){
        return vpkr.deployed().then(function(instance){
            tokenInstance = instance;
            return tokenInstance.transfer.call(accounts[1],999999999999999999999999);
        }).then(assert.fail).catch(function(error){
            assert(error.message.indexOf('revert') >= 0,'Error message must contain revert');
            return tokenInstance.transfer.call(accounts[1],250000,{from: accounts[0]});
        }).then(function(success){
            assert.equal(success,true,'It should return true');
            return tokenInstance.transfer(accounts[1],250000,{from: accounts[0]});
        }).then(function(receipt){
            assert.equal(receipt.logs.length,1,'triggers 1 Event');
            assert.equal(receipt.logs[0].event, 'Transfer','Should have fired the Transfer event');
            assert.equal(receipt.logs[0].args._from,accounts[0],'Should have been transfered from account 0'); 
            assert.equal(receipt.logs[0].args._to,accounts[1],'Should have been transfered to account[1]');
            assert.equal(receipt.logs[0].args._value,250000,'The coin should be 250000');
            return tokenInstance.balances(accounts[1]);
        }).then(function(balance){
            assert.equal(balance.toNumber(),250000,'The transfered amount should be equal to 250000');
            return tokenInstance.balances(accounts[0]);
        }).then(function(balance){
            assert.equal(balance.toNumber(),999750000,'The amount after the transfer should be 750000');
        });
    });

    it('approves token for delegated transfer',function(){
        return vpkr.deployed().then(function(instance){
            tokenInstance = instance;
            return tokenInstance.approve.call(accounts[1],100);
        }).then(function(success){
            assert.equal(success,true,'Approve should return true');
            return tokenInstance.approve(accounts[1],100, {from:accounts[0]});
        }).then(function(receipt){
            assert.equal(receipt.logs.length,1,'triggers 1 Event');
            assert.equal(receipt.logs[0].event, 'Approval','Should have fired the Approval event');
            assert.equal(receipt.logs[0].args._owner,accounts[0],'Should have been approved by account[0]'); 
            assert.equal(receipt.logs[0].args._spender,accounts[1],'Should have been transfered to account[1]');
            assert.equal(receipt.logs[0].args._value,100,'The coin should be 250000');
            return tokenInstance.allowance(accounts[0],accounts[1]);
    }).then(function(allowance){
        assert.equal(allowance,100,'The allowance should have stored 100');
        });
    });

    it('Spends the delegated tokens',function(){
        return vpkr.deployed().then(function(instance){
            tokenInstance = instance;
            fromAccount = accounts[2];
            spendingAccount = accounts[4];
            toAccount = accounts[3];
            return tokenInstance.transfer(fromAccount,100,{from:accounts[0]});
        }).then(function(receipt){ 
            // Receipt gets implemented. We know from above tests that transfer is already working.
            return tokenInstance.approve(spendingAccount,10, {from:fromAccount});
        }).then(function(receipt){
            //Approve method works fine from above test.
            return tokenInstance.transferFrom(fromAccount,toAccount,9999, {from: spendingAccount});
        }).then(assert.fail).catch(function(error){
            assert(error.message.indexOf('revert') >= 0,'cannot transfer 9999 ');
            return tokenInstance.transferFrom(fromAccount,toAccount,20, {from: spendingAccount});
        }).then(assert.fail).catch(function(error){
            assert(error.message.indexOf('revert') >= 0,'cannot transfer 20');
            return tokenInstance.transferFrom.call(fromAccount,toAccount,10, {from: spendingAccount});
        }).then(function(success){
            assert.equal(success,true,'Should return true');
            return tokenInstance.transferFrom(fromAccount,toAccount,10, {from: spendingAccount});
        }).then(function(receipt){
            assert.equal(receipt.logs.length,1,'triggers 1 Event');
            assert.equal(receipt.logs[0].event, 'Transfer','Should have fired the Transfer event');
            assert.equal(receipt.logs[0].args._from,fromAccount,'Should have been transfered from account'); 
            assert.equal(receipt.logs[0].args._to,toAccount,'Should have been transfered to account');
            assert.equal(receipt.logs[0].args._value,10,'The coin should be 10');
            return tokenInstance.balances(fromAccount);
        }).then(function(balance){
            assert.equal(balance.toNumber(),90,'The account balance left should be 90 after the transfer')
            return tokenInstance.balances(toAccount);
        }).then(function(balance){
            assert.equal(balance.toNumber(),10,'The account balance be 10 after the transfer')
            return tokenInstance.allowance(fromAccount,spendingAccount);
        }).then(function(allowance){
            assert.equal(allowance,0,'The account balance be 0 after the transfer')
        });
    }); 
});