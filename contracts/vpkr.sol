pragma solidity ^0.5.0;

// ----------------------------------------------------------------------------
// Safe maths
// ----------------------------------------------------------------------------
contract SafeMath {
    function safeAdd(uint a, uint b) public pure returns (uint c) {
        c = a + b;
        require(c >= a,"C should be greater or equal to a");
        return c;
    }
    function safeSub(uint a, uint b) public pure returns (uint c) {
        require(b <= a,"B should be less or equal to a");
        c = a - b;
        return c;
    }
    function safeMul(uint a, uint b) public pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b,"a should be equal to 0 or c/a should be equal to b");
        return c;
    }
    function safeDiv(uint a, uint b) public pure returns (uint c) {
        require(b > 0,"b should be greater than 0");
        c = a / b;
        return c;
    }
}

// ----------------------------------------------------------------------------
// ERC Token Standard #20 Interface
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-20.md
// ----------------------------------------------------------------------------
contract ERC20Interface {
    function totalSupply() public view returns (uint);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    function allowance(address tokenOwner, address spender) public view returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

// ----------------------------------------------------------------------------
// Owned contract
// ----------------------------------------------------------------------------
contract Owned {
    address public owner;
    address public newOwner;

    event OwnershipTransferred(address indexed _from, address indexed _to);

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        require(msg.sender == owner," Sender Has to be equal to the owner");
        _;
    }

    function transferOwnership(address _newOwner) public onlyOwner {
        newOwner = _newOwner;
    }
    function acceptOwnership() public {
        require(msg.sender == newOwner,"Sender Has to be equal to the new owner");
        emit OwnershipTransferred(owner, newOwner);
        owner = newOwner;
        newOwner = address(0);
    }
}

// ----------------------------------------------------------------------------
// The main Contract
// ----------------------------------------------------------------------------
contract vpkr is ERC20Interface, SafeMath, Owned{
    
    //vairable
    string public name;
    string public symbol;
    string public standard;
    uint8 public decimals;
    uint256 public _totalSupply;
    mapping(address => uint256) public balances;
    mapping(address => mapping(address => uint256)) public allowed;
    
    //Events
    event Approval (
        address indexed _owner,
        address indexed _spender,
        uint256 _value
    );

    event Transfer (
        address indexed _from,
        address indexed _to,
        uint256 _value
    );

    //constructor
    constructor() public {
        name = "vPakistaniRupee";
        symbol = "VPKR";
        standard = "VPKR Token v1.0";
        decimals = 2;
        _totalSupply = 1000000000;
        //alocate the initial supply.
        balances[msg.sender] = _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);
    }

    // ------------------------------------------------------------------------
    // Total supply
    // ------------------------------------------------------------------------
    function totalSupply() public view returns (uint) {
        return _totalSupply; // - balances[address(0)];
    }

    // ------------------------------------------------------------------------
    // Get the token balance for account tokenOwner
    // ------------------------------------------------------------------------
    function balanceOf(address tokenOwner) public view returns (uint balance) {
        return balances[tokenOwner];
    }

    // ------------------------------------------------------------------------
    // Returns the amount of tokens approved by the owner that can be
    // transferred to the spender's account
    // ------------------------------------------------------------------------
    function allowance(address tokenOwner, address spender) public view returns (uint remaining) {
        return allowed[tokenOwner][spender];
    }

    function transfer(address _to, uint256 _value) public returns ( bool success){
        require(balances[msg.sender] >= _value,"This Account doesnt have enough tokens");
        balances[msg.sender] = safeSub(balances[msg.sender], _value);
        balances[_to] = safeAdd(balances[_to], _value);
        emit Transfer(msg.sender,_to,_value);
        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function transferFrom(address _from,address _to, uint256 _value ) public returns (bool success){
        //Checks the Balance before spending. If it gives an error the transfer wont occur
        require(balances[_from] >= _value,"Should have balance more than the value");
        require(allowed[_from][msg.sender] >= _value,"allowance should be more than the value");
        //The actual transfer of tokens is occuring.
        balances[_from] = safeSub(balances[_from], _value);
        balances[_to] = safeAdd(balances[_to], _value);
        //Allowance if getting spent
        allowed[_from][msg.sender] -= _value;
        //The transfer emmits to store the transaction.
        emit Transfer(_from,_to,_value);
        //returns true if everything above gets executed successfully.
        return true;
    }
}